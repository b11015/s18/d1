// alert("hello");

// Functions

	// function printInput(){
	// 	let nickname = prompt("Enter your nickname");
	// 	console.log("Hi " + nickname)
	// };

	// printInput();

	// Parameters and Arguments

	function printName(name){
		console.log("My name is " + name);
	};

	printName(); //result to undefined
	printName("sean");
	printName("Shaun");

	let variableName = "Shawn"
	printName(variableName);

	function checkDivisibilityBy8(num){
		let remainder = num % 8
		console.log("The remainder of " + num + " divided by 8 is " + remainder);

		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	};

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(2548966);

	// Function as Arguments

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed");
	};

	function invokeFunction(argumentFunction){
		argumentFunction();
	};

	invokeFunction(argumentFunction);

// Using multiple parameters
	
	function createFullName(firstName, middleName, lastName){
		console.log(firstName + ' ' + middleName + ' ' + lastName)
	};
	
	createFullName("Jane", "Dela", "Cruz"); //result: Jane Dela Cruz
	createFullName("Cruz", "Jane", "Dela"); //result: Cruz Jane Dela
	// createFullName(prompt("Enter your first name"), prompt("Enter your middle name"), prompt("Enter your last name"));

	createFullName("Jake", "Castro"); // result: Jake Castro undefined

	createFullName("Jean", "Rodriguez", "Ferrer", "Hello") //result: Jean Rodriguez Ferrer not appear Hello because walang parameter
 
	// using multiple variable as multiple arguments
	let firstName = "John"
	let middleName = "Reyes"
	let lastName = "Garcia"

	createFullName(firstName, middleName, lastName)

	function printFullNAme(middleName, firstName, lastName){
		console.log(firstName + ' ' + middleName + ' ' + lastName)
	};

	printFullNAme("Jan", "Asuncion", "Cruz"); //result: Asuncion Jan Cruz

	// return statement
		// usually use it in the last

	function returnFullName(firstName, middleName, lastName){

		return firstName + " " + middleName + " " + lastName
		console.log("This message should not be printed");
	};

	let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
	console.log(completeName) // result: Jeffrey Smith Bezos

	console.log(returnFullName(firstName, middleName, lastName));

	function returnAddress(city, country){

		let fullAdress = city + ', ' + country;
		return fullAdress;

	};

	let myAddress = returnAddress("Pasig City", "Philippines");
	console.log(myAddress);

	function printPlayerInfo(username, level, job){
		// console.log('Username: ' + username);
		// console.log('Level: ' + level);
		// console.log('Job: ' + job);
		return "Username: " + username + ' ' + "Level: " + level + ' ' + "Job: " + job
	};

	let user1 = printPlayerInfo("theTinker", 95, "Warrior");
	console.log(user1);

	// function promptName(name){
	// 	let name = prompt("name")
	// 	return name
	// };

	// function promptNameAgain(promptName){
	// 	promptName(name);
	// 	console.log("hi " + name)
	// };
	// promptNameAgain(promptNameAgain);
